import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule  } from "@angular/common/http";  
// import { HttpClient  } from "@angular/common/http";  
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from "ngx-pagination";
import { Ng2SearchPipeModule } from "ng2-search-filter";

import { SitenavbarComponent } from './components/shared/sitenavbar/sitenavbar.component';
import { SitefooterComponent } from './components/shared/sitefooter/sitefooter.component';
import { BargraphsummaryComponent } from './components/shared/bargraphsummary/bargraphsummary.component';
import { Analysis28daysComponent } from './components/shared/analysis28days/analysis28days.component';
import { AboutComponent } from './components/site/about/about.component';
import { LoginComponent } from './components/site/login/login.component';
import { ToolboxComponent } from './components/site/toolbox/toolbox.component';

import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { DelegatesComponent } from './components/admin/delegates/delegates.component';
import { LongweekendsComponent } from './components/admin/longweekends/longweekends.component';
import { SettingsComponent } from './components/admin/settings/settings.component';
import { UserweightsComponent } from './components/admin/userweights/userweights.component';
import { EngineerlistComponent } from './components/admin/engineer/engineerlist/engineerlist.component';
import { EngineerdetailComponent } from './components/admin/engineer/engineerdetail/engineerdetail.component';
import { EngineeraddComponent } from './components/admin/engineer/engineeradd/engineeradd.component';


import { TaskadminlistComponent } from './components/admin/tasksadmin/taskadminlist/taskadminlist.component';
import { TaskadminaddComponent } from './components/admin/tasksadmin/taskadminadd/taskadminadd.component';
import { TaskadmindetailComponent } from './components/admin/tasksadmin/taskadmindetail/taskadmindetail.component';

import { TaskadmintabComponent } from './components/admin/tasksadmin/taskadmintab/taskadmintab.component';

import { HomeComponent } from './components/home/home.component';
import { MaintabComponent } from './components/tabs/maintab/maintab.component';
import { ResourcesComponent } from './components/tabs/resources/resources.component';
import { ManualtasksComponent } from './components/tabs/manualtasks/manualtasks.component';
import { ActivitylogComponent } from './components/tabs/activitylog/activitylog.component';
import { AutomatedtasksComponent } from './components/tabs/automatedtasks/automatedtasks.component';
import { TasklistComponent } from './components/tasks/tasklist/tasklist.component';
import { TaskdetailComponent } from './components/tasks/taskdetail/taskdetail.component';
import { TaskaddComponent } from './components/tasks/taskadd/taskadd.component';

import { NotfoundComponent } from './components/site/notfound/notfound.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TesterComponent } from './components/site/tester/tester.component';



@NgModule({
  declarations: [
    AppComponent,

    SitenavbarComponent,
    SitefooterComponent,
    BargraphsummaryComponent,
    Analysis28daysComponent,
    AboutComponent,
    LoginComponent,

    DashboardComponent,
    DelegatesComponent,
    LongweekendsComponent,
    UserweightsComponent,
    SettingsComponent,

    HomeComponent,
    MaintabComponent,
    ResourcesComponent,
    ManualtasksComponent,
    ActivitylogComponent,
    AutomatedtasksComponent,

    EngineerlistComponent,
    EngineerdetailComponent,
    EngineeraddComponent,
    TasklistComponent,
    TaskdetailComponent,
    TaskaddComponent,

    TaskadminlistComponent,
    TaskadminaddComponent,
    TaskadmindetailComponent,

    ToolboxComponent,
    TaskadmintabComponent ,
    NotfoundComponent, TesterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    HttpClientModule, 
    NgxPaginationModule , 
    Ng2SearchPipeModule ,
    FormsModule, 
    ReactiveFormsModule ,
    BsDatepickerModule.forRoot() ,
    BrowserAnimationsModule, TabsModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
