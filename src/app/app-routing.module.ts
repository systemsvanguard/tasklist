import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Admin and Generic Site Pages
import { DashboardComponent } from "./components/admin/dashboard/dashboard.component";
import { DelegatesComponent } from "./components/admin/delegates/delegates.component";
import { EngineerlistComponent } from "./components/admin/engineer/engineerlist/engineerlist.component";
import { EngineerdetailComponent } from "./components/admin/engineer/engineerdetail/engineerdetail.component";
import { EngineeraddComponent } from "./components/admin/engineer/engineeradd/engineeradd.component";
import { LongweekendsComponent  } from "./components/admin/longweekends/longweekends.component";
import { SettingsComponent } from "./components/admin/settings/settings.component";
import { UserweightsComponent } from "./components/admin/userweights/userweights.component";
import { AboutComponent } from "./components/site/about/about.component";
import { TesterComponent } from "./components/site/tester/tester.component";
import { ToolboxComponent } from "./components/site/toolbox/toolbox.component";
import { LoginComponent } from "./components/site/login/login.component";
import { HomeComponent } from "./components/home/home.component";
import { TasklistComponent } from "./components/tasks/tasklist/tasklist.component";
import { TaskdetailComponent } from "./components/tasks/taskdetail/taskdetail.component";
import { TaskaddComponent } from "./components/tasks/taskadd/taskadd.component";

import { TaskadminlistComponent } from "./components/admin/tasksadmin/taskadminlist/taskadminlist.component";
import { TaskadmindetailComponent } from "./components/admin/tasksadmin/taskadmindetail/taskadmindetail.component";
import { TaskadminaddComponent } from "./components/admin/tasksadmin/taskadminadd/taskadminadd.component";

import { TaskadmintabComponent } from "./components/admin/tasksadmin/taskadmintab/taskadmintab.component";
import { NotfoundComponent } from "./components/site/notfound/notfound.component";


const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full' },
  {path: 'home', component: HomeComponent },
  {path: 'dashboard', component: DashboardComponent },
  {path: 'delegates', component: DelegatesComponent },
  {path: 'longweekends', component: LongweekendsComponent },
  {path: 'settings', component: SettingsComponent } ,
  {path: 'weights', component: UserweightsComponent } ,
  {path: 'engineers', component: EngineerlistComponent },
  {path: 'engineers/:id', component: EngineerdetailComponent },
  {path: 'listtasktab', component: TaskadmintabComponent } ,
  {path: 'listtask', component: TaskadminlistComponent } ,
  {path: 'listtask/:id', component: TaskadmindetailComponent } ,
  {path: 'listtaskadd', component: TaskadminaddComponent },

  {path: 'tasks', component: TasklistComponent } ,
  {path: 'tasks/:id', component: TaskdetailComponent } ,
  {path: 'tasksadd', component: TaskaddComponent } ,

  {path: 'about', component: AboutComponent },
  {path: 'tester', component: TesterComponent },
  {path: 'toolbox', component: ToolboxComponent } ,
  {path: 'login', component: LoginComponent } ,
  {path: "*", component: NotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
