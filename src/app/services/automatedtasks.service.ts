import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";
// import {Observable} from 'rxjs/Rx';


@Injectable({
  providedIn: 'root'
})
export class AutomatedtasksService {

  // constructor() { }

  private apiServer = "http://localhost:1337/api/engtasks/LOB";
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(private httpClient: HttpClient) { }
  get(): Observable<any[]> {
    // return this.httpClient.get<any[]>(this.apiServer + '/tasks/');
    return this.httpClient.get<any[]>(this.apiServer );
  }

}
