import { TestBed } from '@angular/core/testing';

import { TaskautomatedService } from './taskautomated.service';

describe('TaskautomatedService', () => {
  let service: TaskautomatedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaskautomatedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
