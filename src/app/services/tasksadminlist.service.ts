import { HttpClient, HttpErrorResponse, HttpHeaders } from "@angular/common/http";
import { Injectable } from '@angular/core';
import { catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class TasksadminlistService {

  apiUrl : string = 'https://tasklistapijson.openode.io/tasks';
  headers = new HttpHeaders().set('Content-Type', 'application/json' );

  constructor(private httpClient : HttpClient ) { }

  //----- manage API errors  -------
  handleError(error: HttpErrorResponse ) {
    if (error.error instanceof ErrorEvent ) {
      console.error('An error occurred while accessing API : ', error.error.message );
    }
    else {
      console.error (
        `API backend returned error code ${error.status},  ` + ` body was : ${error.error} `
      );
    }
    return throwError( 'A server error occurred.  Please try again later.'    );
  };
  //--------------------------------

  // CRUD - C | Create a new record  | POST
  create(data: any ): Observable<any> {
    return this.httpClient.post(this.apiUrl, data ).pipe(
      catchError(this.handleError)
    );
  }

  // CRUD - R | Retrieve ALL | GET
  list(): Observable<any> {
    return this.httpClient.get( this.apiUrl).pipe(
      catchError(this.handleError )
    );
  }

  listv1(): Observable<any> {
    return this.httpClient.get( 'http://localhost:1337/api/engtasks/LOB').pipe(
      catchError(this.handleError )
    );
  }

  // CRUD - R | Retrieve specific record | GET by ID
  getItem(id: any): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}/${id}`).pipe(
      catchError(this.handleError)
    );
  }

  // CRUD - U | Update specific record by ID | PUT
  update(id: any, data: any ): Observable<any>  {
    return this.httpClient.put(`${this.apiUrl}/${id}`, data ).pipe(
      catchError(this.handleError)
    );
  }

  // CRUD - D | Delete/ remove specific record by ID | DELETE
  delete(id: any ): Observable<any> {
    return this.httpClient.delete(`${this.apiUrl}/${id}`).pipe(
      catchError(this.handleError )
    );
  }

  // search by name
  filterByTitle(taskname: any ): Observable<any> {
    return this.httpClient.get(`${this.apiUrl}?title_like=${taskname}`).pipe(
      catchError(this.handleError)
    );
  }


}
