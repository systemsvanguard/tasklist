import { TestBed } from '@angular/core/testing';

import { TaskmanualService } from './taskmanual.service';

describe('TaskmanualService', () => {
  let service: TaskmanualService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TaskmanualService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
