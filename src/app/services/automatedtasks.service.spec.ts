import { TestBed } from '@angular/core/testing';

import { AutomatedtasksService } from './automatedtasks.service';

describe('AutomatedtasksService', () => {
  let service: AutomatedtasksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AutomatedtasksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
