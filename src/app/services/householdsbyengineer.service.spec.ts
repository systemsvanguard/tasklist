import { TestBed } from '@angular/core/testing';

import { HouseholdsbyengineerService } from './householdsbyengineer.service';

describe('HouseholdsbyengineerService', () => {
  let service: HouseholdsbyengineerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HouseholdsbyengineerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
