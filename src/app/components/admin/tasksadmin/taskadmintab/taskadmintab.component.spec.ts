import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskadmintabComponent } from './taskadmintab.component';

describe('TaskadmintabComponent', () => {
  let component: TaskadmintabComponent;
  let fixture: ComponentFixture<TaskadmintabComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskadmintabComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskadmintabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
