import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { TasksadminlistService } from "src/app/services/tasksadminlist.service";

@Component({
  selector: 'app-taskadmindetail',
  templateUrl: './taskadmindetail.component.html',
  styleUrls: ['./taskadmindetail.component.css']
})
export class TaskadmindetailComponent implements OnInit {


  currentTask: any;
  message = '';

  constructor(
    private tasksService: TasksadminlistService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getTask(this.route.snapshot.paramMap.get('id'));
  }

  getTask(id: string | null): void {
    this.tasksService.getItem(id)
      .subscribe(
        (task: null) => {
          this.currentTask = task;
          console.log(task);
        },
        (error: any) => {
          console.log(error);
        });
  }



  setAvailableStatus(status: any): void {
    const data = {
      name: this.currentTask.taskname,
      description: this.currentTask.description,
      tasktype: this.currentTask.tasktype,
      weight: this.currentTask.weight,
      available: status
    };

    this.tasksService.update(this.currentTask.id, data)
      .subscribe(
        response => {
          this.currentTask.available = status;
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }

  updateTask(): void {
    this.tasksService.update(this.currentTask.id, this.currentTask)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'This task was updated.';
        },
        error => {
          console.log(error);
        });
  }

  deleteTask(): void {
    this.tasksService.delete(this.currentTask.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/listtask']);
        },
        error => {
          console.log(error);
        });
  }


}
