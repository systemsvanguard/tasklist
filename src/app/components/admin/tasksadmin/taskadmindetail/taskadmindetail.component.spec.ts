import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskadmindetailComponent } from './taskadmindetail.component';

describe('TaskadmindetailComponent', () => {
  let component: TaskadmindetailComponent;
  let fixture: ComponentFixture<TaskadmindetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskadmindetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskadmindetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
