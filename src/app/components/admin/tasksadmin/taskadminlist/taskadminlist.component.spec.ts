import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskadminlistComponent } from './taskadminlist.component';

describe('TaskadminlistComponent', () => {
  let component: TaskadminlistComponent;
  let fixture: ComponentFixture<TaskadminlistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TaskadminlistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskadminlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
