import { Component, OnInit } from '@angular/core';
import { TasksadminlistService } from "src/app/services/tasksadminlist.service";

@Component({
  selector: 'app-taskadminlist',
  templateUrl: './taskadminlist.component.html',
  styleUrls: ['./taskadminlist.component.css']
})
export class TaskadminlistComponent implements OnInit {

  tasks: any;
  currentTask:any;
  currentIndex = -1;
  searchTitle = '';

  constructor(private tasksService: TasksadminlistService) { }

  ngOnInit(): void {
    this.getAllTasks();
  }


  // Get list
  getAllTasks(): void {
    this.tasksService.listv1()
      .subscribe(
        (tasks: any) => {
          this.tasks = tasks;
        },
        (error: any) => {
          console.log(error);
        });
  }

  // Delete action
  deleteTask(id:number){
    this.tasksService.delete(id)
    .subscribe(
      response => {
        this.getAllTasks();
      },
      error => {
        console.log(error);
      });
  }


  // Search items
  searchByTitle(): void {
    this.tasksService.filterByTitle(this.searchTitle)
      .subscribe(
        tasks => {
          this.tasks = tasks;
        },
        error => {
          console.log(error);
        });
  }


}
