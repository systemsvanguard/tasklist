import { Component, OnInit } from '@angular/core';
import { TasksadminlistService } from "src/app/services/tasksadminlist.service";

@Component({
  selector: 'app-taskadminadd',
  templateUrl: './taskadminadd.component.html',
  styleUrls: ['./taskadminadd.component.css']
})
export class TaskadminaddComponent implements OnInit {

  task = {
    taskname: '',
    description: '',
    tasktype: '',
    weight: ''
  };
  isTaskAdded = false;

  constructor(private tasksService: TasksadminlistService) { }

  ngOnInit(): void {
  }

  // Add New
  addTask(): void {
    const data = {
      taskname: this.task.taskname,
      description: this.task.description ,
      tasktype: this.task.tasktype ,
      weight: this.task.weight
    };
    if (!data.taskname) {
      alert('Please add task.');
      return;
    }

    this.tasksService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.isTaskAdded = true;
        },
        error => {
          console.log(error);
        });
  }

  // Reset on adding new
  newTask(): void {
    this.isTaskAdded = false;
    this.task = {
      taskname: '',
      description: '',
      tasktype: '',
      weight: ''
    };
  }

}
