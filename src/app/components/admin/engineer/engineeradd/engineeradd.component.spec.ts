import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EngineeraddComponent } from './engineeradd.component';

describe('EngineeraddComponent', () => {
  let component: EngineeraddComponent;
  let fixture: ComponentFixture<EngineeraddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EngineeraddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EngineeraddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
