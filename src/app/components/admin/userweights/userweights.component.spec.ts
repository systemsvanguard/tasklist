import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserweightsComponent } from './userweights.component';

describe('UserweightsComponent', () => {
  let component: UserweightsComponent;
  let fixture: ComponentFixture<UserweightsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserweightsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserweightsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
