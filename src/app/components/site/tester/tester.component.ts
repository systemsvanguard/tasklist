import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-tester',
  templateUrl: './tester.component.html',
  styleUrls: ['./tester.component.css']
})
export class TesterComponent implements OnInit {

  public hhbypas2 : any;  // hhbypa ---> HouseHold by PA 
  constructor( private http: HttpClient ) {
  }

  getHHbyPA2() { 
    const apiHHbyPA2 ='http://localhost:1337/api/engtasks/LOB' ; 
    // const apiHHbyPA2 ='https://tasklistapijson.openode.io/TMB/' ; 
    this.http.get<any>(apiHHbyPA2).subscribe((res) => {
      this.hhbypas2  =  res.incomplete;  
      console.log(this.hhbypas2)
    }  )
  }

  getHHbyPA3() {
    const apiHHbyPA2 ='http://localhost:1337/api/engtasks/LOB' ; 
    // const apiHHbyPA2 ='https://tasklistapijson.openode.io/LOB/' ; 
    this.http.get<any>(apiHHbyPA2).subscribe((res) => {
      this.hhbypas2  =  res.incomplete;    
      console.log(this.hhbypas2)
    }  )
  }

  getHHbyPA4() {
    const apiHHbyPA2 ='http://localhost:1337/api/engtasks/NAN' ; 
    // const apiHHbyPA2 ='https://tasklistapijson.openode.io/NAN/' ; 
    this.http.get<any>(apiHHbyPA2).subscribe((res) => {
      this.hhbypas2  =  res.incomplete;    
      console.log(this.hhbypas2)
    }  )
  }

  ngOnInit() {
    this.getHHbyPA2() 
  }

}
