import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-toolbox',
  templateUrl: './toolbox.component.html',
  styleUrls: ['./toolbox.component.css']
})
export class ToolboxComponent implements OnInit {

  public tasksbyhh : any;  // Tasks by Household 
  
  constructor( private http: HttpClient ) {
  }
  
  ngOnInit() {
    this.getTasksbyHH() 
  }
  
  
  getTasksbyHH() { 
    const current_pa = 'lob';
    const apiTasksbyHH =`http://localhost:1337/api/engtasks/${current_pa}` ; 
    this.http.get<any>(apiTasksbyHH).subscribe((res) => {
      this.tasksbyhh  =  res.incomplete;  
      console.log(this.tasksbyhh)
    }  )
  }

}
