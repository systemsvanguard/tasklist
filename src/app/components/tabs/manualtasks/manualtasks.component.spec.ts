import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManualtasksComponent } from './manualtasks.component';

describe('ManualtasksComponent', () => {
  let component: ManualtasksComponent;
  let fixture: ComponentFixture<ManualtasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManualtasksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManualtasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
