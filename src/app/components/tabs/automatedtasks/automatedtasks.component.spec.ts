import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutomatedtasksComponent } from './automatedtasks.component';

describe('AutomatedtasksComponent', () => {
  let component: AutomatedtasksComponent;
  let fixture: ComponentFixture<AutomatedtasksComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AutomatedtasksComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutomatedtasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
