// import { Component, OnInit } from '@angular/core';
import { Component, OnInit, Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})


@Component({
  selector: 'app-automatedtasks',
  templateUrl: './automatedtasks.component.html',
  styleUrls: ['./automatedtasks.component.css']
})
export class AutomatedtasksComponent implements OnInit {

  // hhbypa ~ HouseHold by PA 
  public hhbypas : any ;  
  public hhbypas_incompl : any ; 
  public hhbypas_compl : any ; 
  public hhbypas_flag : any ; 
  public tasksbyhh : any;  // Tasks by Household 

  //------ pagination & search for household list ------>
  // pagination package : https://www.npmjs.com/package/ngx-pagination 
  p: number = 1;
  // total: number = 10;
  
  // search filter package "ng2-search-filter": https://www.npmjs.com/package/ng2-search-filter  
  searchterm = '';

  //------------------------> 

  constructor( private http: HttpClient ) {
  }

  ngOnInit() {
    this.getHHbyPA_incomplete() 
    , this.getHHbyPA_complete() 
    , this.getHHbyPA_flag() 
    , this.getTasksbyHH()  
  }

  //------------------------->
  getTasksbyHH() { 
    const current_pa = 'lob';
    const apiTasksbyHH =`http://localhost:1337/api/engtasks/${current_pa}` ; 
    this.http.get<any>(apiTasksbyHH).subscribe((res) => {
      this.tasksbyhh  =  res.incomplete;  
      console.log(this.tasksbyhh)
    }  )
  }

  // Incomplete Tasks - households by PA
  getHHbyPA_incomplete() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/LOB' ; 
    // const apiHHbyPA ='https://tasklistapijson.openode.io/LOB/' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_incompl  =  res.incomplete  
      console.log(this.hhbypas_incompl)
    }  )
  }


  getHHbyPA_LOB_incomplete() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/LOB' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_incompl  =  res.incomplete  
      console.log(this.hhbypas_incompl)
    }  )
  }
  getHHbyPA_TMB_incomplete() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/TMB' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_incompl  =  res.incomplete  
      console.log(this.hhbypas_incompl)
    }  )
  }
  getHHbyPA_NAN_incomplete() {
    let engineer = 'nan'; 
    const apiHHbyPA ='http://localhost:1337/api/engtasks/${id}`' ; // /${id}`
    // const apiHHbyPA = 'http://localhost:1337/api/engtasks/NAN' ;
    // const apiHHbyEngineer = `http://localhost:1337/api/engtasks/${engineer}` ;
    const apiHHbyEngineer = `http://localhost:1337/api/engtasks/${engineer}` ;
    // this.http.get<any>(apiHHbyPA).subscribe((res) => {
    this.http.get<any>(apiHHbyEngineer).subscribe((res) => {
      this.hhbypas_incompl  =  res.incomplete  
      console.log(this.hhbypas_incompl)
    }  )
  }

  // Complete Tasks - households by PA
  getHHbyPA_complete() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/LOB' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_compl  =  res.completed  
      console.log(this.hhbypas_compl)
    }  )
  }
  getHHbyPA_LOB_complete() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/LOB' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_compl  =  res.completed  
      console.log(this.hhbypas_compl)
    }  )
  }
  getHHbyPA_TMB_complete() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/TMB' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_compl  =  res.completed  
      console.log(this.hhbypas_compl)
    }  )
  }
  getHHbyPA_NAN_complete() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/NAN' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_compl  =  res.completed  
      console.log(this.hhbypas_compl)
    }  )
  }


  // Flagged Tasks - households by PA
  getHHbyPA_flag() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/LOB' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_flag  =  res.flagged  
      console.log(this.hhbypas_flag)
    }  )
  }
  getHHbyPA_LOB_flag() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/LOB' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_flag  =  res.flagged  
      console.log(this.hhbypas_flag)
    }  )
  }
  getHHbyPA_TMB_flag() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/TMB' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_flag  =  res.flagged  
      console.log(this.hhbypas_flag)
    }  )
  }
  getHHbyPA_NAN_flag() {
    const apiHHbyPA ='http://localhost:1337/api/engtasks/NAN' ; 
    this.http.get<any>(apiHHbyPA).subscribe((res) => {
      this.hhbypas_flag  =  res.flagged  
      console.log(this.hhbypas_flag)
    }  )
  }
 

}
