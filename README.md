# Numeris Single Source Task List

Numeris Single Source Task List management application.  This is the front-end, built using Angular 12, Fontawesome icons, and Bootstrap v 5 CSS framework.  It consumes and gets data from a RESTful API backend using MS SQL.  It allows us to display, add, search, and manage Single Source Tasks.

## Numeris Task List Application :
- Each Task has id, title, tasktype, weight, description, published status.
- We can create, retrieve, update, delete Tasks.
- There is a Search bar for finding Tasks by title.



## Steps to Install
- Run the command below from the command line / terminal / command prompt.
- git clone https://systemsvanguard@bitbucket.org/systemsvanguard/tasklist.git
- cd tasklist\ 
- ensure your have Node & NPM pre-installed. Run commands 'node --version && npm -v'.
- npm install.  (This ensures all dependencies are installed).
- Ensure to rename local ".env4Display" to ".env".  
- Start the server with "ng serve --port 4200" 
- Runs on port 4200 --> http://localhost:4200/. 


## Features
- Angular 12 framework
- Bootstrap v5 CSS framework
- FontAwesome 5 icons
- Google Fonts
- Filler text from https://pirateipsum.me
- dotenv & environment variables


## License
This project is licensed under the terms of the **MIT** license.


## Screenshot
![Screen 01](http://www.ryanhunter.ca/images/portfolio/single_source_api/screen01.png) 

